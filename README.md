# Treaty identifiers

This project develops resources for correctly identifying treaties in structured and unstructured data, so as to facilitate the merging of different datasets for reproducible legal data science.

The automated combination of treaty data from different sources is currently somewhat of a challenge because data providers typically do not use the same identifiers (not even treaty titles are always the same). Likewise, references to treaties in academic or other writing often cannot automatically be detected and disambiguated. Ideally these issues would be resolved at the source, but for the time being this repository provides a spreadsheet and code for matching and merging treaty-related datasets.

Whether or not the respective datasets can and should be used in a given research project is another question altogether, this is only about the means for matching and merging. 

The project is still at an early stage and feedback, ideas and collaborators are welcome!


## About the data
Currently only two datasets are covered, but more will be added in the future. The two are:

1) The United Nations Treaty Series [(UNTS)](https://treaties.un.org/Pages/Content.aspx?path=DB/UNTS/pageIntro_en.xml), by far the largest and most authoritative collection of international agreements of the UN era, containing about 70'000 agreements, crawled with the [UNTS-searchbot](https://gitlab.com/legalinformatics/unts-searchbot)

2) The International Environmental Agreements Database ([IEADB](https://iea.uoregon.edu/)), probably the largest academic database of international environmental agreements, with 5814 rows in its treaties spreadsheet [db_treaties.csv](https://iea.uoregon.edu/file/dbtreatiescsv) from 2022-12-26


## Related projects

The project [treaty-references](https://gitlab.com/martinakunz/treaty-references) uses the automatically retrieved UNTS data to generate a bibliographic database and files with OSCOLA-style treaty references for use in publications.

The [IEADB-scraper](https://gitlab.com/legalinformatics/ieadb-scraper) fetches treaty texts given a set of text URLs.

More on treaty data science sources and tools at https://martinakunz.gitlab.io/treaty-analytics/.


## How it works
Merging two (or more) datasets about treaties requires finding a single, or more likely, combination of data items that is the same in each dataset and can serve as the key for a merge/join.

The IEADB's above-mentioned `db_treaties.csv` file has a column called 'UNTS', but only 77 rows out of the 5814 have a non-empty cell in that column, and only 25 of them are actually UNTS references (the others are mostly references to national treaty series), so this is of little use.

The only potential overlap between UNTS index data and the IEADB is some combination of treaty title and treaty adoption date (entry into force date is not included in UNTS index data). The title alone would not be sufficient because both datasets contain a sizeable number of duplicate treaty titles (e.g. the title "Agreement concerning financial co-operation." occurs over 500 times in UNTS).

Matching by title and adoption date is not as straightforward as it might seem, either. Many bilateral treaties in UNTS have a list of dates under 'Date of Conclusion', because the agreement was probably established by an exchange of notes or instruments, whereas IEADB only has one date per agreement. To attempt a match regardless, the first adoption date in UNTS is extracted and converted to ISO format (YYYY-MM-DD). As for agreement titles, there are many whitespace, case, punctuation, parentheses and spelling differences. The current matching script (see below) could probably be substantially improved upon, as it yields an intersection of only 456 treaty records (453 unique UNTS records, corresponding to 456 IEADB records),[^1] 194 of which have an agreement text in IEADB.

The columns of the `auto-matched-treaties.csv` file are as follows:
- `UNTStreatyTitle` : Treaty title exactly as retrieved with UNTS-searchbot (called 'Title' in the original dataset)
- `UNTStreatyAdoptionDate` : Treaty adoption date(s) exactly as shown in UNTS ('Date of Conclusion')
- `UNTSregNb` : UNTS registration number as scraped by UNTS-searchbot ('Registration Number')  -- not unique to each treaty
- `UNTSvolNb` : UNTS volume number(s) from UNTS-searchbot ('UNTS Volume number') -- a long list in some cases
- `UNTStreatyRecordURL` : UNTS treaty page URL (embedded in the treaty title in the online source)
- `titlenad` : The normalized title and formatted (first) adoption date used for automatically matching treaty records
- `IEADBid` : The id (number) given by the IEA database (column called 'IEA# (click for add'l info)' in the source) 
- `IEADBagreementName` : 'Agreement Name' in IEADB
- `IEADBsignatureDate` : Column called 'Signature Date' in IEADB (but corresponding to the adoption/conclusion date, not the date of signing or opening a treaty for signatures in cases where there two differ)
- `IEADBtextURL` : generated by concatenating 'https://iea.uoregon.edu/treaty-text/' with the IEADBid for treaties that have a text in the IEADB[^2]

The code used to match the two datasets and produce `auto-matched-treaties.csv` is:

```python
import os
import numpy as np
import pandas as pd

# load datasets
untsindex = pd.read_csv(os.path.expanduser('~/git/phd/untsindex.csv'), encoding='utf-8', low_memory=False)
ieadb = pd.read_csv(os.path.expanduser('~/Downloads/db_treaties.csv'), encoding='utf-8')
# remove spaces in column names
ieadb.columns = ieadb.columns.str.replace(' ','')
ieadb.rename({"IEA#(clickforadd'linfo)":'ieaid'}, axis='columns', inplace=True)
# only keep rows with a treaty title
untsindex = untsindex[untsindex.title.notna()].reset_index(drop=True)
# lower casing and whitespace normalization
untsindex['titlematch'] = untsindex.title.str.lower().str.replace('\s+',' ', regex=True)
ieadb['titlematch'] = ieadb.AgreementName.str.lower().str.replace('\s+',' ', regex=True)
# remove anything that isn't a word, number or whitespace character
untsindex.titlematch = untsindex.titlematch.str.replace('[^\w\s]+','',regex=True).str.strip()
ieadb.titlematch = ieadb.titlematch.str.replace('[^\w\s]+','',regex=True).str.strip()
# some bilateral treaties have more than one date in the adoptDate column 
untsindex['firstAdoptDate'] = untsindex.adoptDate.str.split(',').str[0]
# parse as time series (converting to ISO format)
untsindex.firstAdoptDate = pd.to_datetime(untsindex.firstAdoptDate, dayfirst=True)
untsindex = untsindex.sort_values(by='firstAdoptDate').reset_index(drop=True)
# create title + adoption date identifier
untsindex['titlenad'] = untsindex.titlematch + '_' + untsindex.firstAdoptDate.astype(str)
ieadb['titlenad'] = ieadb.titlematch + '_' + ieadb.SignatureDate
# take subsets from both datasets
untssub = untsindex.loc[untsindex.titlenad.isin(ieadb.titlenad), ['title','adoptDate','regNb','UNTSvolNb','UNTSurl','titlenad']]
ieadbsub = ieadb.loc[ieadb.titlenad.isin(untsindex.titlenad), ['ieaid','AgreementName','SignatureDate','titlenad','TreatyText']]
# merge the two subsets based on the merge key
newsub = untssub.merge(ieadbsub, how='outer',on='titlenad')
# generate IEADB text URL
newsub['IEADBtextURL'] = 'https://iea.uoregon.edu/treaty-text/' + newsub.ieaid.astype(str)
# remove text URLs where TreatyText is NA
newsub.loc[newsub.TreatyText.isna(), 'IEADBtextURL'] = np.nan
# rename columns to clarify origin of data
newsub.rename(columns={'title':'UNTStreatyTitle','adoptDate':'UNTStreatyAdoptionDate','regNb':'UNTSregNb','UNTSurl':'UNTStreatyRecordURL','ieaid':'IEADBid','AgreementName':'IEADBagreementName','SignatureDate':'IEADBsignatureDate'}, inplace=True, errors='raise')
# select columns for CSV output
newsub = newsub[['UNTStreatyTitle', 'UNTStreatyAdoptionDate', 'UNTSregNb', 'UNTSvolNb', 'UNTStreatyRecordURL', 'titlenad', 'IEADBid', 'IEADBagreementName', 'IEADBsignatureDate', 'IEADBtextURL']]
# save subset to CSV
newsub.to_csv(os.path.expanduser('~/git/treaty-identifiers/auto-matched-treaties.csv'), encoding='utf-8', index=False)
```

[^1]: Three IEADB records seem to be duplicates, in that the metadata is substantially the same as that of three other IEADB records with different IDs (7139, 7265, 7594, 8213, 8804, and 8806).

[^2]: Whether or not the IEADB contains a text for a given agreement is determined based on the 'Treaty Text' column in the source CSV file. In the matching script, the textURL is first generated for all the rows, and then those rows that have an empty cell (parsed as NA for Not Available) in that column are set to NA as well. 
